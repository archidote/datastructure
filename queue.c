/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "queue.h" // file 
void init_queue(Queue q) {// constructeur de (l'objet) Stack s 
    for (int i = 0; i < QUEUE_MAX_SIZE; i++) { // intialisation du tableau 
    q->data[i] = 0.0; 
    } 
    q->index =  0 ; // indice du tableau qui part de 0  
}

void enqueue(Queue q, float value) { 
    q->data[q->index] = value; 
};

float dequeue(Queue q) {
    float tempSortie = q->data[0];
    
    for (int i = 0; i < q->index-1; i++) { 
        q->data[i] = q->data[i+1];
    }    
    return tempSortie;   
}; // ça dégage 

bool is_queue_empty(Queue q){
    if ( q->index == 0 ) {
        return true;
    }
    else {
        return false;
    } 
};

float front(Queue q) {
    return q->data[q->index-1];  
}; //aka peek

void clear(Queue q){
        q->index = 0;
};

/* int factorielle(int value) {
    int temp = 1;
    for (int i = 1; i <= value; i++) { 
        temp = (i*i++) * value; 
    }
}*/

