/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "stack.h"
void init_stack(Stack s) { // constructeur de (l'objet) Stack s 
    for (int i = 0; i < STACK_MAX_SIZE; i++) { // intialisation du tableau 
    s->data[i] = 0.0; 
    } 
    s->index =  0 ; // indice du tableau qui part de 0  
}

void push(Stack s, float value) { // il met la value dans la première case vide
    s->data[s->index];
    s->index++;   
}

float pop(Stack s) { // récupère la dernière valeur de la stack et la met de côté ; 
    s->index-- ;
    return s->data[s->index];    
}

bool is_stack_empty(Stack s) { 
    if ( s->index == 0 ) {
        return true;
    }
    else {
        return false;
    }
}
float peek(Stack s) {
    return s->data[s->index-1];  
} // aka top or front

void dup(Stack s) {
    if ( s->index == 0 ) {
        printf("Erreur ");
    }
    else {
        s->data[s->index] = s->data[s->index-1];
    }
       
}
void swap(Stack s) { 
    if ( s->index >= 2 ) {
        float temp = 0 ; 
        temp = s->data[s->index-1] // 16 dans temp 
        s->data[s->index-1] = s->data[s->index-2]; // 8 dans 16
        s->data[s->index-2] = temp ; 
    }
    else { 
        printf("Erreur ");
    }
    
}
void clear(Stack s) { // on remet à l'index à 0
    s->index = 0;
}
